package main

import (
	"bufio"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/aibou2/go-server/pkg/frontend/core"
	"os"
	"strings"
)

func main() {
	logrus.SetLevel(logrus.DebugLevel)
	fmt.Println("go-server text frontend")
	t, err := core.NewText()
	if err != nil {
		logrus.Fatal(err)
	}
	var line []byte
	var in string
	var out []string
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("< ")
		line, _, err = reader.ReadLine()
		//_, err = fmt.Scanf("%s\n", &in)
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "reading: %s", err)
		}
		in = string(line)
		out, err = t.Message(in, "text frontend")
		if err != nil {
			if err.Error() == "EOF" {
				os.Exit(0)
			}
			_, _ = fmt.Fprintf(os.Stderr, "backend: %s", err)
		}
		for _, msg := range out {
			fmt.Println("> " + strings.ReplaceAll(msg, "\n", "\n  "))
		}
	}
}
