package main

import (
	"flag"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/aibou2/go-server/pkg/frontend/rest"
)

func main() {
	var port int
	flag.IntVar(&port, "port", -1, "port to serve on")
	flag.Parse()

	logrus.Fatal(rest.NewServer().Run(fmt.Sprintf(":%v", port)))
}
