package line

import (
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jessevdk/go-flags"
	"github.com/line/line-bot-sdk-go/linebot"
	"github.com/pelletier/go-toml"
	"github.com/sirupsen/logrus"
	"golang.org/x/xerrors"
	"io/ioutil"
	"os"
)

//import "github.com/line/line-bot-sdk-go/linebot"
var config Config
var opts Opts

func Main() {
	logrus.SetLevel(logrus.TraceLevel)
	setupArgs()
	config = setupConfig(opts.ConfigPath)
	//setupDB()
	setupAndRunServer()
}

func setupLineBot() (client *linebot.Client, err error) {
	client, err = linebot.New(
		os.Getenv("AIBOU_GO_SERVER_LINE_CHANNEL_SECRET"),
		os.Getenv("AIBOU_GO_SERVER_LINE_CHANNEL_TOKEN"),
	)
	return
}

func setupAndRunServer() {
	var corsConfig *cors.Config

	if config.CORS != nil {
		logrus.WithFields(logrus.Fields{
			"config": config.CORS,
		}).Info("cors is enabled")

		corsConfig = &cors.Config{
			AllowAllOrigins:        config.CORS.AllowAllOrigins,
			AllowOrigins:           config.CORS.AllowOrigins,
			AllowMethods:           config.CORS.AllowMethods,
			AllowHeaders:           config.CORS.AllowHeaders,
			AllowCredentials:       config.CORS.AllowCredentials,
			ExposeHeaders:          config.CORS.ExposeHeaders,
			MaxAge:                 config.CORS.MaxAge,
			AllowWildcard:          config.CORS.AllowWildcard,
			AllowBrowserExtensions: config.CORS.AllowBrowserExtensions,
			AllowWebSockets:        config.CORS.AllowWebSockets,
		}
	} else {
		logrus.Info("cors is disabled")
	}

	lineBot, err := setupLineBot()
	if err != nil {
		logrus.Fatal(err)
	}

	e := NewServer(corsConfig, lineBot)
	logrus.Fatal(e.Run(fmt.Sprintf(":%d", opts.Port)))
}

//func setupDB() {
//	switch {
//	case config.DB.Redis != nil:
//		opt := godis.Option{
//			Host:              config.DB.Redis.Host,
//			Port:              config.DB.Redis.Port,
//			ConnectionTimeout: config.DB.Redis.ConnectionTimeout,
//			SoTimeout:         config.DB.Redis.SoTimeout,
//			Password:          config.DB.Redis.Password,
//			Db:                config.DB.Redis.Db,
//		}
//		logrus.Debugf("Redis Options: %+v", opt)
//		auth.JWTAuthBackend = auth.NewRedis(opt)
//		db.BackendToUse = db.NewRedis(opt)
//		logrus.WithFields(logrus.Fields{
//			"config": config.DB.Redis,
//		}).Info("db connected")
//	case config.DB.Postgres != nil:
//		var err error
//		db.BackendToUse, err = db.NewSQL("postgres", fmt.Sprintf(
//			"user=%s password=%s host=%s port=%d dbname=%s sslmode=%s",
//			config.DB.Postgres.User,
//			config.DB.Postgres.Pass,
//			config.DB.Postgres.Host,
//			config.DB.Postgres.Port,
//			config.DB.Postgres.DB,
//			config.DB.Postgres.SSLMode,
//		))
//		if err != nil {
//			logrus.WithFields(logrus.Fields{
//				"config": config.DB.Postgres,
//				"error":  err,
//			}).Fatal("db conn failed")
//		} else {
//			logrus.WithFields(logrus.Fields{
//				"config": config.DB.Postgres,
//			}).Info("db connected")
//		}
//	default:
//		logrus.Fatal("no db config provided")
//	}
//}

func setupArgs() {
	_, err := flags.ParseArgs(&opts, os.Args)
	if err != nil {
		logrus.Fatal(err)
	}

	switch opts.Log {
	case "panic", "p":
		logrus.SetLevel(logrus.PanicLevel)
	case "fatal", "f":
		logrus.SetLevel(logrus.FatalLevel)
	case "error", "e":
		logrus.SetLevel(logrus.ErrorLevel)
	case "warn", "w":
		logrus.SetLevel(logrus.WarnLevel)
	case "info", "i":
		logrus.SetLevel(logrus.InfoLevel)
	case "debug", "d":
		logrus.SetLevel(logrus.DebugLevel)
	case "trace", "t":
		logrus.SetLevel(logrus.TraceLevel)
	}

	if opts.Release {
		logrus.Info("Release Mode")
		gin.SetMode(gin.ReleaseMode)
		logrus.Infof("Gin Mode: %s", gin.Mode())
		logrus.SetLevel(logrus.InfoLevel)
		logrus.SetFormatter(&logrus.JSONFormatter{})
		// default report caller is false
	} else {
		logrus.Info("Debug Mode")
		gin.SetMode(gin.DebugMode)
		logrus.Infof("Gin Mode: %s", gin.Mode())
		logrus.SetLevel(logrus.TraceLevel)
		// default formatter is text so not needed
		logrus.SetReportCaller(true)
	}
}

func setupConfig(path string) (config Config) {
	var src []byte
	var err error
	if path == "" {
		logrus.Warn(`no config path provided. defaulting to envvar`)
		src = []byte(os.Getenv("AIBOU_GO_SERVER_LINE_CONFIG"))
	} else {
		src, err = ioutil.ReadFile(path)
		if err != nil {
			logrus.Fatal(xerrors.Errorf("reading config file: %w", err))
		}
	}
	logrus.Debug("config:", string(src))
	err = toml.Unmarshal(src, &config)
	if err != nil {
		logrus.Fatal(xerrors.Errorf("reading config: %w", err))
	}
	return
}
