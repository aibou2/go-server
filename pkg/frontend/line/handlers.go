package line

import (
	"github.com/gin-gonic/gin"
	"github.com/line/line-bot-sdk-go/linebot"
	"github.com/sirupsen/logrus"
	data2 "gitlab.com/aibou2/go-server/pkg/data"
	"gitlab.com/aibou2/go-server/pkg/frontend/core"
	"net/http"
)

func Meta(c *gin.Context) {
	c.JSON(http.StatusOK, data2.Meta)
}

func (s *Server) handleTextMessage(event *linebot.Event, message *linebot.TextMessage) {
	userID := event.Source.UserID
	s.textsLock.Lock()
	defer s.textsLock.Unlock()
	if _, ok := s.texts[userID]; !ok {
		t, err := core.NewText()
		if err != nil {
			logrus.Error(err)
			s.textsLock.Unlock()
			return
		}
		s.texts[userID] = t
	}
	t := s.texts[userID]

	msgs, err := t.MessageLINE(message.Text, userID)
	if err != nil {
		logrus.Error(err)
		return
	}

	lineMsgs := make([]linebot.SendingMessage, len(msgs))
	for i, msg := range msgs {
		lineMsgs[i] = msg
	}

	if _, err = s.line.PushMessage(userID, lineMsgs...).Do(); err != nil {
		logrus.Error(err)
	}
}

func (s *Server) handleWebhook(ctx *gin.Context) {
	// some parts parts may be copy-pasta'd from here:
	// https://github.com/line/line-bot-sdk-go/blob/v7.8.0/examples/echo_bot/server.go

	events, err := s.line.ParseRequest(ctx.Request)
	if err != nil {
		if err == linebot.ErrInvalidSignature {
			ctx.JSON(http.StatusForbidden, nil)
		} else {
			ctx.JSON(http.StatusInternalServerError, nil)
		}
		return
	}

	for _, event := range events {
		if event.Type == linebot.EventTypeMessage {
			switch message := event.Message.(type) {
			case *linebot.TextMessage:
				s.handleTextMessage(event, message)
			}
		}
	}
}
