package line

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/line/line-bot-sdk-go/linebot"
	"gitlab.com/aibou2/go-server/pkg/frontend/core"
	"net/http"
	"sync"
)

type Server struct {
	*gin.Engine
	line      *linebot.Client
	texts     map[string]*core.Text
	textsLock sync.Mutex
}

func NewServer(corsConfig *cors.Config, lineBot *linebot.Client) (s *Server) {
	s = &Server{Engine: gin.New(), line: lineBot, texts: map[string]*core.Text{}}

	// Setup CORS if enabled
	if corsConfig != nil {
		s.Use(cors.New(*corsConfig))
	}

	// Setup other middleware
	s.Use(
		gin.Logger(),
		gin.Recovery(),
	)

	v1 := s.Group("/v1")
	v1.POST("/webhook", s.handleWebhook)
	v1.GET("/meta", Meta)
	v1.GET("/status", func(ctx *gin.Context) {
		// for shields.io
		ctx.JSON(http.StatusOK, gin.H{
			"status": "ok",
		})
	})

	return
}
