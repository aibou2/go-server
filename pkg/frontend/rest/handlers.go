package rest

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/aibou2/go-server/pkg/backend/types"
	data2 "gitlab.com/aibou2/go-server/pkg/data"
	"net/http"
)

func Meta(c *gin.Context) {
	c.JSON(http.StatusOK, data2.Meta)
}

var _ gin.HandlerFunc = Meta

type AskReq struct {
	Conversation types.Conversation `json:"conversation"`
}

func Ask(c *gin.Context) {
	var req AskReq
	var err error

	if err := c.BindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, types.Resp{
			Msg: err.Error(),
		})
	}
	req.Conversation, err = req.Conversation.Ask(c.Request.Context())
	if err != nil {
		c.JSON(http.StatusInternalServerError, types.Resp{
			Msg: "error",
		})
		logrus.Errorf("while handling request: %s", err)
		return
	}
	c.JSON(http.StatusOK, types.Resp{
		Msg:  "ok",
		Data: req.Conversation,
	})
}

var _ gin.HandlerFunc = Ask
