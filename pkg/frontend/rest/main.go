// Package rest provides a REST-like server frontend.
// It does authentication using pkg/backend/auth .

package rest

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/aibou2/go-server/pkg/backend/auth/auth"
	"gitlab.com/aibou2/go-server/pkg/backend/data"
)

type Server struct {
	*gin.Engine
}

func NewServer() (s *Server) {
	s = &Server{
		Engine: gin.New(),
	}

	// Setup JWT Authentication Middleware
	authMiddleware := auth.SetupJWT(s.Engine, true)

	// Groups
	verify := s.Group("/", authMiddleware)

	// Routes
	s.GET("/", Meta)
	verify.POST("/ask", Ask)

	return
}

func (s *Server) Recommended() {
	s.Use(
		gin.Logger(),
		gin.Recovery(),
		cors.New(data.CORSConfig),
	)
}
