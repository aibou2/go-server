# AIbou Go Server REST Frontend

## Usage

### Setup

1. configure
2. build
3. run with *binary*` --port=`*port*.

### Routes

Routes with `*` require authentication.

`GET  /` for metadata
`POST /ask` for asking `*`

`GET  /auth` for auth status `*`
`POST /auth` for auth
`POST /auth/refresh` for refreshing auth `*`
