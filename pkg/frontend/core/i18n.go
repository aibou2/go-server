package core

import (
	"embed"
	"github.com/nicksnyder/go-i18n/v2/i18n"
	"github.com/pelletier/go-toml"
	"golang.org/x/text/language"
	"io/fs"
)

//go:embed i18n/*
var messageFiles embed.FS

var Bundle *i18n.Bundle
var Localizer *i18n.Localizer

func init() {
	Bundle = i18n.NewBundle(language.English)

	Bundle.RegisterUnmarshalFunc("toml", toml.Unmarshal)
	matches, err := fs.Glob(messageFiles, "i18n/active.*.toml")
	if err != nil {
		panic(err)
	}
	var b []byte
	for _, match := range matches {
		b, err = messageFiles.ReadFile(match)
		if err != nil {
			panic(err)
		}
		_, err := Bundle.ParseMessageFileBytes(b, match)
		if err != nil {
			panic(err)
		}
	}

	Localizer = i18n.NewLocalizer(Bundle, language.Japanese.String(), language.English.String())
}
