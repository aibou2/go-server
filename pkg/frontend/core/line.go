package core

import (
	"fmt"
	"github.com/line/line-bot-sdk-go/linebot"
	"gitlab.com/aibou2/go-server/pkg/backend/external"
	"strconv"
)

func lineScenario(s external.Scenario) *linebot.BubbleContainer {
	container, err := linebot.UnmarshalFlexMessageJSON([]byte(fmt.Sprintf(
		`{
  "type": "bubble",
  "header": {
    "type": "box",
    "layout": "vertical",
    "contents": [
      {
        "type": "text",
        "text": "Scenario %d",
        "size": "lg"
      }
    ]
  },
  "body": {
    "type": "box",
    "layout": "vertical",
    "contents": [
      {
        "type": "box",
        "layout": "horizontal",
        "contents": [
          {
            "type": "box",
            "layout": "vertical",
            "contents": [
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                  {
                    "type": "text",
                    "text": "Level"
                  },
                  {
                    "type": "text",
                    "text": "%d"
                  }
                ]
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                  {
                    "type": "text",
                    "text": "Duration"
                  },
                  {
                    "type": "text",
                    "text": "%d"
                  }
                ]
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                  {
                    "type": "text",
                    "text": "AI"
                  },
                  {
                    "type": "text",
                    "text": %s
                  }
                ]
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                  {
                    "type": "text",
                    "text": "You"
                  },
                  {
                    "type": "text",
                    "text": %s
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "type": "box",
        "layout": "vertical",
        "contents": [
          {
            "type": "text",
            "text": %s,
            "wrap": true
          }
        ]
      }
    ]
  },
  "footer": {
    "type": "box",
    "layout": "vertical",
    "contents": [
      {
        "type": "button",
        "action": {
          "type": "message",
          "label": "Start",
          "text": "/start %d"
        }
      }
    ]
  }
}`,
		s.ID,
		s.Level,
		s.Duration,
		strconv.Quote(s.AiName),
		strconv.Quote(s.HumanName),
		strconv.Quote(s.Description+"\n\n"+s.Mission+"\n\n"+s.Information+"\n\n"+s.Info),
		s.ID,
	)))
	if err != nil {
		panic(err)
	}
	return container.(*linebot.BubbleContainer)
}

func LineScenario(s external.Scenario) *linebot.FlexMessage {
	return linebot.NewFlexMessage(s.String(), lineScenario(s))
}

func LineScenarios(scenarios []external.Scenario) *linebot.FlexMessage {
	containers := make([]*linebot.BubbleContainer, len(scenarios))
	for i, scenario := range scenarios {
		containers[i] = lineScenario(scenario)
	}

	return linebot.NewFlexMessage("I'm so sorry the alt text wouldn't fit in.", &linebot.CarouselContainer{
		Contents: containers,
	})
}

func LineConversation(c external.Conversation) *linebot.FlexMessage {
	container, err := linebot.UnmarshalFlexMessageJSON([]byte(fmt.Sprintf(
		`{
  "type": "bubble",
  "body": {
    "type": "box",
    "layout": "horizontal",
    "contents": [
      {
        "type": "box",
        "layout": "vertical",
        "contents": [
          {
            "type": "text",
			"wrap": true,
            "text": "Conversation %d"
          }
        ]
      }
    ]
  },
  "footer": {
    "type": "box",
    "layout": "vertical",
    "contents": [
      {
        "type": "button",
        "action": {
          "type": "message",
          "label": "End",
          "text": "/end"
        }
      }
    ]
  }
}`,
		c.ID,
	)))
	if err != nil {
		panic(err)
	}
	return linebot.NewFlexMessage(fmt.Sprintf("Error: %s", err), container)
}

func LineMeta(version, built string) *linebot.FlexMessage {
	container, err := linebot.UnmarshalFlexMessageJSON([]byte(fmt.Sprintf(
		`{
  "type": "bubble",
  "body": {
    "type": "box",
    "layout": "vertical",
    "contents": [
      {
        "type": "text",
        "text": "AIbou Go Server"
      },
      {
        "type": "box",
        "layout": "horizontal",
        "contents": [
          {
            "type": "button",
            "action": {
              "type": "uri",
              "label": "Website",
              "uri": "https://furutamototada.wixsite.com/website"
            }
          },
          {
            "type": "button",
            "action": {
              "type": "uri",
              "label": "Source",
              "uri": "https://gitlab.com/aibou2/go-server"
            }
          }
        ]
      },
      {
        "type": "box",
        "layout": "horizontal",
        "contents": [
          {
            "type": "text",
            "text": "Version"
          },
          {
            "type": "text",
			"wrap": true,
            "text": %s
          }
        ]
      },
      {
        "type": "box",
        "layout": "horizontal",
        "contents": [
          {
            "type": "text",
            "text": "Build Time"
          },
          {
            "type": "text",
			"wrap": true,
            "text": %s
          }
        ]
      },
      {
        "type": "box",
        "layout": "horizontal",
        "contents": [
          {
            "type": "text",
            "text": "Contributors"
          },
          {
            "type": "text",
            "text": "Ken Shibata",
            "wrap": true
          }
        ]
      }
    ]
  }
}`,
		strconv.Quote(version),
		strconv.Quote(built),
	)))
	if err != nil {
		panic(err)
	}
	return linebot.NewFlexMessage(fmt.Sprintf("Error: %s", err), container)
}

func LineMessage(msg external.Message) linebot.SendingMessage {
	return LineText(msg.Text).WithSender(linebot.NewSender(msg.Name, "https://obs.line-scdn.net/0hu0pyfKNyKh9NADwOJ-xVSGxdIX1-YjQUb2ZifWwJcCllMGkneWVhe20FIS9mMmQadjRheQYAfH0yNGpPcSNkLDsDfXswYw/f256x256"))
}

func LineText(msg string) *linebot.TextMessage {
	return linebot.NewTextMessage(msg)
}

func LineInfo(msg string) *linebot.TextMessage {
	return LineText(fmt.Sprintf("ℹ️ %s", msg))
}

func LineError(msg string) *linebot.TextMessage {
	return LineText(fmt.Sprintf("❌ %s", msg))
}

func LineWarning(msg string) *linebot.TextMessage {
	return LineText(fmt.Sprintf("⚠️ %s", msg))
}
