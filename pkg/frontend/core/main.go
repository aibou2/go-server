package core

import (
	"fmt"
	"github.com/line/line-bot-sdk-go/linebot"
	"github.com/nicksnyder/go-i18n/v2/i18n"
	"github.com/sirupsen/logrus"
	"gitlab.com/aibou2/go-server/pkg/backend/external"
	data2 "gitlab.com/aibou2/go-server/pkg/data"
	"net/http"
	"runtime/debug"
	"strconv"
	"strings"
	"time"
)

type Text struct {
	scenario           external.Scenario
	conversation       external.Conversation
	conversationActive bool
	server             *external.Server
	made               string
}

func NewText() (*Text, error) {
	server, err := external.NewServer(external.BaseURL.String(), &http.Client{Timeout: 5 * time.Second}, password)
	if err != nil {
		return nil, err
	}
	return &Text{server: server, made: time.Now().String()}, nil
}

const password = "pass"

func (t *Text) Message(content string, userId string) ([]string, error) {
	_ = t.server.Wake()
	args := strings.Split(strings.TrimSpace(strings.ToLower(content)), " ")
	command := args[0]
	switch command {
	case "/start":
		// Choose Scenario
		if len(args) < 2 {
			msgs, err := t.Message("/scenarios", userId)
			if err != nil {
				logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
				return []string{
					Localizer.MustLocalize(&i18n.LocalizeConfig{
						MessageID: "Error",
					}),
				}, nil
			}
			return append([]string{Localizer.MustLocalize(&i18n.LocalizeConfig{
				MessageID: "NoScenarioProvided",
			})}, msgs...), nil
		}
		id, err := strconv.ParseInt(args[1], 10, 32)
		if err != nil {
			logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
			return []string{
				Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "Error",
				}),
			}, nil
		}
		scenarios, err := t.server.Scenarios()
		if err != nil {
			logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
			return []string{
				Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "Error",
				}),
			}, nil
		}
		if len(scenarios) == 0 {
			return []string{fmt.Sprintf(
				"Error No Scenarios: %s",
				err,
			)}, nil
		}
		found := false
		for _, scenario := range scenarios {
			if scenario.ID == int(id) {
				found = true
				t.scenario = scenario
				break
			}
		}
		if !found {
			return []string{fmt.Sprintf(
				"Error No Scenarios found with ID: %d",
				id,
			)}, nil
		}

		// Start Conversation
		conversation, err := t.server.StartConversation(external.StartConversationReq{
			UserID:     userId,
			ScenarioId: t.scenario.ID,
			Password:   password,
		})
		if err != nil {
			logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
			return []string{
				Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "Error",
				}),
			}, nil
		}
		t.conversation = conversation
		t.conversationActive = true
		msgs, err := t.server.Logs(t.conversation, userId)
		if err != nil {
			logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
			return []string{
				Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "Error",
				}),
			}, nil
		}
		return append([]string{"A conversation will start now. To end the conversation, send \"/end\".", t.conversation.String()}, msgs.Strings()...), nil
	case "/end":
		// Choose Scenario
		if len(args) < 2 {
			msgs, err := t.Message("/scenarios", userId)
			if err != nil {
				logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
				return []string{
					Localizer.MustLocalize(&i18n.LocalizeConfig{
						MessageID: "Error",
					}),
				}, nil
			}
			return append([]string{Localizer.MustLocalize(&i18n.LocalizeConfig{
				MessageID: "NotInConversation",
			})}, msgs...), nil
		}
		msg := t.conversation.StringNoScenario()
		t.conversation = external.Conversation{}
		t.conversationActive = false
		return []string{fmt.Sprintf(
			"%s ended.", msg,
		), "Please check out our app: https://apps.apple.com/jp/app/aibou/id1567793044"}, nil
	case "/meta":
		return []string{data2.Meta.String()}, nil
	case "/scenarios":
		scenarios, err := t.server.Scenarios()
		if err != nil {
			logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
			return []string{
				Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "Error",
				}),
			}, nil
		}
		scenarioStrings := make([]string, len(scenarios))
		for i, scenario := range scenarios {
			scenarioStrings[i] = scenario.String()

			logrus.Debug(scenario.String())
		}

		return append([]string{Localizer.MustLocalize(&i18n.LocalizeConfig{
			MessageID: "Scenarios",
		})}, scenarioStrings...), nil
	default:
		if !t.conversationActive {
			msgs, err := t.Message("/scenarios", userId)
			if err != nil {
				logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
				return []string{
					Localizer.MustLocalize(&i18n.LocalizeConfig{
						MessageID: "Error",
					}),
				}, nil
			}
			return append([]string{
				Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "NotInConversation",
				}),
			}, msgs...), nil
		}
		resp, err := t.server.Chat(external.ChatReq{
			ConversationId: t.conversation.ID,
			UserInput:      content,
			Password:       password,
			UserID:         userId,
		})
		if err != nil {
			logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
			return []string{
				Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "Error",
				}),
			}, nil
		}
		switch resp.Response.Safety {
		case external.SafetyIgnore, external.SafetySafe:
			return []string{
				resp.Response.Text,
			}, nil
		case external.SafetySensitive:
			return []string{
				Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "MessageSensitive",
				}),
				resp.Response.Text,
			}, nil
		case external.SafetyUnsafe:
			return []string{
				Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "MessageUnsafe",
				}),
				resp.Response.Text,
			}, nil
		default:
			return []string{
				resp.Response.Text,
			}, nil
		}
	}
}

func (t *Text) MessageLINE(content string, userId string) ([]linebot.SendingMessage, error) {
	_ = t.server.Wake()
	args := strings.Split(strings.TrimSpace(strings.ToLower(content)), " ")
	command := args[0]
	switch command {
	case "/start":
		// Choose Scenario
		if len(args) < 2 {
			msgs, err := t.MessageLINE("/scenarios", userId)
			if err != nil {
				logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
				return []linebot.SendingMessage{
					LineError(Localizer.MustLocalize(&i18n.LocalizeConfig{
						MessageID: "Error",
					})),
				}, nil
			}
			return append([]linebot.SendingMessage{LineInfo(Localizer.MustLocalize(&i18n.LocalizeConfig{
				MessageID: "NoScenarioProvided",
			}))}, msgs...), nil
		}
		id, err := strconv.ParseInt(args[1], 10, 32)
		if err != nil {
			return []linebot.SendingMessage{
				LineError("Integer not provided."),
			}, nil
		}
		scenarios, err := t.server.Scenarios()
		if err != nil {
			logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
			return []linebot.SendingMessage{
				LineError(Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "Error",
				})),
			}, nil
		}
		if len(scenarios) == 0 {
			return []linebot.SendingMessage{
				LineError("No scenarios available."),
			}, nil
		}
		found := false
		for _, scenario := range scenarios {
			if scenario.ID == int(id) {
				found = true
				t.scenario = scenario
				break
			}
		}
		if !found {
			return []linebot.SendingMessage{
				LineError(fmt.Sprintf(
					"No Scenarios with ID %s found.",
					err,
				)),
			}, nil
		}

		// Start Conversation
		conversation, err := t.server.StartConversation(external.StartConversationReq{
			UserID:     userId,
			ScenarioId: t.scenario.ID,
			Password:   password,
		})
		if err != nil {
			logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
			return []linebot.SendingMessage{
				LineError(Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "Error",
				})),
			}, nil
		}
		t.conversation = conversation
		t.conversationActive = true
		msgs, err := t.server.Logs(t.conversation, userId)
		if err != nil {
			logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
			return []linebot.SendingMessage{
				LineError(Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "Error",
				})),
			}, nil
		}
		lineMsgs := make([]linebot.SendingMessage, len(msgs))
		for i, msg := range msgs {
			lineMsgs[i] = LineMessage(msg)
		}
		return append([]linebot.SendingMessage{LineInfo(Localizer.MustLocalize(&i18n.LocalizeConfig{
			MessageID: "StartConversation",
		})), LineConversation(t.conversation)}, lineMsgs...), nil
	case "/end":
		if !t.conversationActive {
			return []linebot.SendingMessage{LineWarning("Since you are not in a conversation, the message will not be read by an AI.")}, nil
		}
		msg := t.conversation.StringNoScenario()
		t.conversation = external.Conversation{}
		t.conversationActive = false
		return []linebot.SendingMessage{LineInfo(fmt.Sprintf(
			"%s ended.", msg,
		)), LineInfo("Please check out our app: https://apps.apple.com/jp/app/aibou/id1567793044")}, nil
	case "/meta":
		return []linebot.SendingMessage{LineMeta(data2.Version, data2.Built)}, nil
	case "/scenarios":
		scenarios, err := t.server.Scenarios()
		if err != nil {
			logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
			return []linebot.SendingMessage{
				LineError(Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "Error",
				})),
			}, nil
		}
		return append([]linebot.SendingMessage{LineText(Localizer.MustLocalize(&i18n.LocalizeConfig{
			MessageID: "Scenarios",
		}))}, LineScenarios(scenarios)), nil
	default:
		if !t.conversationActive {
			msgs, err := t.MessageLINE("/scenarios", userId)
			if err != nil {
				logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
				return []linebot.SendingMessage{
					LineError(Localizer.MustLocalize(&i18n.LocalizeConfig{
						MessageID: "Error",
					})),
				}, nil
			}
			return append([]linebot.SendingMessage{
				LineWarning(Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "NotInConversation",
				})),
			}, msgs...), nil
		}
		resp, err := t.server.Chat(external.ChatReq{
			ConversationId: t.conversation.ID,
			UserInput:      content,
			Password:       password,
			UserID:         userId,
		})
		if err != nil {
			logrus.WithField("text", t).WithField("stack", string(debug.Stack())).Error(err)
			return []linebot.SendingMessage{
				LineError(Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "Error",
				})),
			}, nil
		}
		switch resp.Response.Safety {
		case external.SafetyIgnore, external.SafetySafe:
			return []linebot.SendingMessage{
				LineMessage(resp.Response),
			}, nil
		case external.SafetySensitive:
			return []linebot.SendingMessage{
				LineWarning(Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "MessageSensitive",
				})),
				LineMessage(resp.Response),
			}, nil
		case external.SafetyUnsafe:
			return []linebot.SendingMessage{
				LineWarning(Localizer.MustLocalize(&i18n.LocalizeConfig{
					MessageID: "MessageUnsafe",
				})),
				LineMessage(resp.Response),
			}, nil
		default:
			return []linebot.SendingMessage{
				LineMessage(resp.Response),
			}, nil
		}
	}
}
