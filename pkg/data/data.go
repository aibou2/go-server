package data

import (
	_ "embed"
	"fmt"
	"net/url"
	"os"
)

const Name = "AIbou Go Server"

//go:embed version.txt
var Version string

//go:embed contributors.txt
var Contributors string

//go:embed built.txt
var Built string

var AppName = os.Getenv("HEROKU_APP_NAME")
var AppURL *url.URL
var V1URL *url.URL
var IconURL *url.URL

func init() {
	var err error
	AppURL, err = url.Parse(fmt.Sprintf("https://%s.herokuapp.com", AppName))
	if err != nil {
		panic(err)
	}
	V1URL, err = url.Parse(fmt.Sprintf("https://%s.herokuapp.com/v1", AppName))
	if err != nil {
		panic(err)
	}
	IconURL, err = url.Parse("/static/icon.png")
	if err != nil {
		panic(err)
	}
}

type MetaType struct {
	Name         string
	Version      string
	Contributors string
	Built        string
}

var _ fmt.Stringer = MetaType{}

func (m MetaType) String() string {
	return fmt.Sprintf(
		"%s\nhttps://gitlab.com/aibou2/go-server\nVersion %s\nAuthors:\n%s\nBuilt on %s",
		m.Name,
		m.Version,
		m.Contributors,
		m.Built,
	)
}

var Meta = MetaType{
	Name:         Name,
	Version:      Version,
	Contributors: Contributors,
	Built:        Built,
}
