// Package openai provides bindings to OpenAI's GPT-3 API.

package openai

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"github.com/sirupsen/logrus"

	"golang.org/x/xerrors"
)

var DefaultBaseURL *url.URL

const contentFilterEngine = "content-filter-alpha-c4"

func init() {
	var err error
	DefaultBaseURL, err = url.Parse("https://api.openai.com")
	if err != nil {
		panic(err)
	}
	logrus.Debugf("Base URL is %s", DefaultBaseURL)
}

type Client struct {
	client  *http.Client
	apiKey  string
	orgKey  *string
	baseURL *url.URL
}

func NewClient(client *http.Client, apiKey string, orgKey *string) *Client {
	return &Client{
		client:  client,
		apiKey:  apiKey,
		orgKey:  orgKey,
		baseURL: DefaultBaseURL,
	}
}

func (c *Client) addHeaders(header http.Header) {
	header.Set("Authorization", fmt.Sprintf("Bearer %s", c.apiKey))
	if c.orgKey != nil {
		header.Set("OpenAI-Organization", *c.orgKey)
	}
	header.Set("Content-Type", "application/json")
}

func (c *Client) doReq(ctx context.Context, method string, URI *url.URL, reqBody interface{}, respBody interface{}) error {
	var err error
	defer func() {
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"URL":   c.baseURL.ResolveReference(URI).String(),
				"req":   reqBody,
				"defer": true,
				"err":   err,
			}).Error("OpenAI API failed request")
		}
	}()
	logrus.WithFields(logrus.Fields{
		"URL": c.baseURL.ResolveReference(URI).String(),
		"req": reqBody,
	}).Debug("OpenAI API doing request")
	reqBuf := bytes.NewBuffer(nil)
	err = json.NewEncoder(reqBuf).Encode(reqBody)
	if err != nil {
		return err
	}
	req, err := http.NewRequestWithContext(
		ctx,
		method,
		c.baseURL.ResolveReference(URI).String(),
		reqBuf,
	)
	if err != nil {
		return err
	}
	c.addHeaders(req.Header)

	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(respBody)
	if err != nil {
		return err
	}
	logrus.WithFields(logrus.Fields{
		"URL":  c.baseURL.ResolveReference(URI).String(),
		"req":  reqBody,
		"resp": respBody,
		"err":  err,
	}).Debug("OpenAI API done request")
	return nil
}

type CreateCompletionReq struct {
	Engine           string         `json:"-"`
	Prompt           *string        `json:"prompt,omitempty"`
	MaxTokens        *int           `json:"max_tokens,omitempty"`
	Temperature      *float32       `json:"temperature,omitempty"`
	TopP             *float32       `json:"top_p,omitempty"`
	N                *int           `json:"n,omitempty"`
	Stream           *bool          `json:"stream,omitempty"`
	Logprobs         *int           `json:"logprobs,omitempty"`
	Stop             *string        `json:"stop,omitempty"`
	PresencePenalty  *float32       `json:"presence_penalty,omitempty"`
	FrequencyPenalty *float32       `json:"frequency_penalty,omitempty"`
	BestOf           *int           `json:"best_of,omitempty"`
	LogitBias        map[string]int `json:"logit_bias,omitempty"`
	User             string         `json:"user"`
}

var _ fmt.Stringer = CreateCompletionReq{}

func (c CreateCompletionReq) String() string {
	b, _ := json.Marshal(c)
	return string(b)
}

type APIError struct {
	Message string      `json:"message"`
	Type    string      `json:"type"`
	Param   interface{} `json:"param"`
	Code    interface{} `json:"code"`
}

var _ error = APIError{}

func (a APIError) Error() string {
	re := fmt.Sprintf("%s: %s", a.Type, a.Message)
	if a.Code != nil {
		re += fmt.Sprintf(" (code: %+v)", a.Code)
	}
	if a.Param != nil {
		re += fmt.Sprintf(" (param: %+v)", a.Param)
	}
	return re
}

type CreateCompletionResp struct {
	//ID      string `json:"id"`
	//Object  string `json:"object"`
	//Created int    `json:"created"`
	//Model   string `json:"model"`
	//Choices []struct {
	//	Text     string `json:"text"`
	//	Index    int    `json:"index"`
	//	Logprobs struct {
	//		Tokens        []string  `json:"tokens"`
	//		TokenLogprobs []float64 `json:"token_logprobs"`
	//		TopLogprobs   []float64 `json:"top_logprobs"`
	//		TextOffset    []int     `json:"text_offset"`
	//	} `json:"logprobs"`
	//	FinishReason string `json:"finish_reason"`
	//} `json:"choices"`
	ID      string `json:"id"`
	Object  string `json:"object"`
	Created int    `json:"created"`
	Model   string `json:"model"`
	Choices []struct {
		Text     string `json:"text"`
		Index    int    `json:"index"`
		Logprobs struct {
			Tokens        []string             `json:"tokens"`
			TokenLogprobs []float64            `json:"token_logprobs"`
			TopLogprobs   []map[string]float64 `json:"top_logprobs"`
			TextOffset    []int                `json:"text_offset"`
		} `json:"logprobs"`
		FinishReason string `json:"finish_reason"`
	} `json:"choices"`
	Error *APIError `json:"error"`
}

var _ fmt.Stringer = CreateCompletionResp{}

func (c CreateCompletionResp) String() string {
	b, _ := json.Marshal(c)
	return string(b)
}

func (c *Client) CreateCompletion(ctx context.Context, req CreateCompletionReq) (resp CreateCompletionResp, err error) {
	if c.apiKey == "" {
		return CreateCompletionResp{
			ID: "test",
			Choices: []struct {
				Text     string `json:"text"`
				Index    int    `json:"index"`
				Logprobs struct {
					Tokens        []string             `json:"tokens"`
					TokenLogprobs []float64            `json:"token_logprobs"`
					TopLogprobs   []map[string]float64 `json:"top_logprobs"`
					TextOffset    []int                `json:"text_offset"`
				} `json:"logprobs"`
				FinishReason string `json:"finish_reason"`
			}{
				{Text: "test response"},
			},
		}, err
	}

	URI, err := url.Parse(fmt.Sprintf("v1/engines/%s/completions", req.Engine))
	if err != nil {
		return
	}
	err = c.doReq(ctx, http.MethodPost, URI, req, &resp)
	if err != nil {
		return
	}

	if resp.Error != nil {
		err = resp.Error
		// may return error and resp, depends on OpenAI API
		return
	}
	return
}

type UnexpectedInputError struct {
	Type      string
	Want, Got interface{}
}

var _ error = UnexpectedInputError{}

func (u UnexpectedInputError) Error() string {
	return fmt.Sprintf("unexpected input (%s): want %s, got %s", u.Type, u.Want, u.Got)
}

func (c *Client) ContentFilter(ctx context.Context, text string, user string) (level int, err error) {
	defer func() {
		if err != nil {
			err = xerrors.Errorf("content filter: %w", err)
		}
	}()

	level = -1 // make sure -1 is returned when level is not set (since that is an error/bug)

	// Make API call
	var rawResp CreateCompletionResp
	prompt := fmt.Sprintf("<|endoftext|>%s\n--\nLabel:", text)
	maxTokens := 1
	temperature := float32(0.0)
	topP := float32(0.0)
	logprobs := 1
	rawResp, err = c.CreateCompletion(ctx, CreateCompletionReq{
		Engine:      contentFilterEngine,
		Prompt:      &prompt,
		MaxTokens:   &maxTokens,
		Temperature: &temperature,
		TopP:        &topP,
		Logprobs:    &logprobs,
		User:        user,
	})
	if err != nil {
		return
	}

	// Verify data
	if len(rawResp.Choices) != 1 {
		err = UnexpectedInputError{
			Type: "slice-length",
			Want: 1,
			Got:  len(rawResp.Choices),
		}
		return
	}

	// Check logprobs of response
	const threshold = -0.355
	choice := rawResp.Choices[0]
	switch choice.Text {
	case "0":
		level = 0
	case "1":
		level = 1
	case "2":
		// TODO Verify length of TopLogprobs
		logprobs := choice.Logprobs.TopLogprobs[0]
		if logprobs["2"] < threshold {
			// "not sufficiently confident in 2"
			logprob0, ok0 := logprobs["0"]
			logprob1, ok1 := logprobs["1"]
			if ok0 && ok1 {
				switch logprob0 >= logprob1 {
				case true:
					level = 0
				case false:
					level = 1
				}
			} else if ok0 {
				level = 0
			} else if ok1 {
				level = 1
			}
		}
	}
	switch level {
	case 0, 1, 2:
	default:
		level = 2
	}
	return
}
