package openai

type Renderer interface {
	Render() string
}
