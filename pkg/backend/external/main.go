package external

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var DebugMode bool

func init() {
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp:   true,
		TimestampFormat: time.RFC3339Nano,
	})
}

type Server struct {
	BaseURL  *url.URL
	Client   *http.Client
	Password string
}

var (
	urlAuthenticate      = "/demo/conversation/authentication"
	urlStartConversation = "/demo/conversation/"
	urlScenarios, _      = url.Parse("/demo/conversation/scenarios")
	urlRefresh, _        = url.Parse("/demo/conversation/reload")
	urlLogs, _           = url.Parse("/demo/conversation/log/view")
	urlChat              = "/demo/conversation/chat/"
	BaseURL, _           = url.Parse("https://convbackend.herokuapp.com")
)

func NewServer(baseURL string, client *http.Client, password string) (*Server, error) {
	u, err := url.Parse(baseURL)
	if err != nil {
		return nil, err
	}
	if client == nil {
		return nil, errors.New("client must not be nil")
	}
	return &Server{BaseURL: u, Client: client, Password: password}, nil
}

type Button struct {
	Button string `json:"button"`
	Action int    `json:"action"`
}

type Conversation struct {
	ID       int      `json:"conversation_id"`
	Type     string   `json:"type"`
	Msg      string   `json:"msg"`
	Scenario Scenario `json:"scenario_data"`
}

func (c Conversation) String() string {
	return c.StringNoScenario() + ":\n" + c.Scenario.String()
}

func (c Conversation) StringNoScenario() string {
	return fmt.Sprintf("(c%d) Conversation %d", c.ID, c.ID)
}

type Message struct {
	Id           int      `json:"id"`
	Conversation int      `json:"conversation"`
	Scenario     Scenario `json:"scenario"`
	Type         string   `json:"type"`
	Name         string   `json:"name"`
	Text         string   `json:"text"`
	Visible      bool     `json:"visible"`
	Editable     bool     `json:"editable"`
	Send         bool     `json:"send"`
	IncludeName  bool     `json:"include_name"`
	LogNumber    int      `json:"log_number"`
	Safety       int      `json:"safety"`
}

var Safety = map[int]string{
	-1: "-", // doesn't matter
	0:  "s", // Safe
	1:  "e", // sEnsitive
	2:  "u", // Unsafe
}

const (
	SafetyIgnore    = -1
	SafetySafe      = 0
	SafetySensitive = 1
	SafetyUnsafe    = 2
)

func (m Message) String() string {
	if DebugMode {
		flags := ""
		if m.Visible {
			flags += "v"
		} else {
			flags += "-"
		}
		if m.Editable {
			flags += "e"
		} else {
			flags += "-"
		}
		if m.IncludeName {
			flags += "i"
		} else {
			flags += "-"
		}

		flags += Safety[m.Safety]
		return fmt.Sprintf("(m%s-%d-%d) %s: %s", flags, m.Conversation, m.Id, m.Name, m.Text)
	}
	return fmt.Sprintf("%s: %s", m.Name, m.Text)
}

type Messages []Message

func (m Messages) String() string {
	re := ""
	for _, m2 := range m {
		re += m2.String() + "\n"
	}
	return re
}

func (m Messages) Strings() []string {
	re := make([]string, len(m))
	for i, m2 := range m {
		re[i] = m2.String()
	}
	return re
}

type LogsReq struct {
	ConversationId int    `json:"conversation_id"`
	Password       string `json:"password"`
	UserID         string `json:"user_id"`
}

func (s *Server) Wake() error {
	_, err := s.Client.Get(s.BaseURL.String())
	return err
}

func (s *Server) Logs(c Conversation, userID string) (Messages, error) {
	body, err := json.Marshal(LogsReq{
		ConversationId: c.ID,
		Password:       s.Password,
		UserID:         userID,
	})
	if err != nil {
		return nil, err
	}
	resp, err := s.Client.Post(s.BaseURL.ResolveReference(urlLogs).String(), "application/json", bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}
	re := make([]Message, 0)
	b, err := ioutil.ReadAll(resp.Body)
	err = json.NewDecoder(bytes.NewBuffer(b)).Decode(&re)
	if err != nil {
		return nil, err
	}
	return re, nil
}

type AuthenticateReq struct {
	UserID   string `json:"user_id"`
	Password string `json:"password"`
}

func (s *Server) Authenticate(req AuthenticateReq) ([]Conversation, error) {
	body, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	resp, err := s.Client.Post(s.BaseURL.String()+urlAuthenticate, "application/json", bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}
	re := make([]Conversation, 0)
	b, err := ioutil.ReadAll(resp.Body)
	fmt.Println(string(b))
	err = json.NewDecoder(bytes.NewBuffer(b)).Decode(&re)
	if err != nil {
		return nil, err
	}
	return re, nil
}

type StartConversationReq struct {
	UserID     string `json:"user_id"`
	ScenarioId int    `json:"scenario_id"`
	Password   string `json:"password"`
}

func (s *Server) StartConversation(req StartConversationReq) (Conversation, error) {
	if req.Password == "" {
		req.Password = s.Password
	}

	body, err := json.Marshal(req)
	if err != nil {
		return Conversation{}, err
	}
	resp, err := s.Client.Post(
		s.BaseURL.String()+urlStartConversation, "application/json", bytes.NewBuffer(body))
	if err != nil {
		return Conversation{}, err
	}
	re := Conversation{}
	b, err := ioutil.ReadAll(resp.Body)
	err = json.NewDecoder(bytes.NewBuffer(b)).Decode(&re)
	if err != nil {
		return Conversation{}, err
	}
	if strings.HasPrefix(re.Type, "error.") {
		return Conversation{}, fmt.Errorf("%s: %s", re.Type, re.Msg)
	}
	return re, nil
}

type ChatReq struct {
	ConversationId int    `json:"conversation_id"`
	UserInput      string `json:"user_input"`
	Password       string `json:"password"`
	UserID         string `json:"user_id"`
}

type ChatResp struct {
	Type     string  `json:"type"`
	Message  string  `json:"msg"`
	Response Message `json:"response"`
}

func (s *Server) Chat(req ChatReq) (ChatResp, error) {
	// TODO: handle errors (especially unsafe text)
	body, err := json.Marshal(req)
	if err != nil {
		return ChatResp{}, err
	}
	resp, err := s.Client.Post(s.BaseURL.String()+urlChat, "application/json", bytes.NewBuffer(body))
	if err != nil {
		return ChatResp{}, err
	}
	re := ChatResp{}
	b, err := ioutil.ReadAll(resp.Body)
	err = json.NewDecoder(bytes.NewBuffer(b)).Decode(&re)
	if err != nil {
		return ChatResp{}, err
	}
	return re, nil
}

type ScenariosReq struct {
	Password string `json:"password"`
}

type Scenario struct {
	ID                  int    `json:"id"`
	Title               string `json:"title"`
	Description         string `json:"description"`
	Duration            int    `json:"duration"`
	Level               int    `json:"level"`
	Information         string `json:"information"`
	Mission             string `json:"mission"`
	Caution             string `json:"caution"`
	ControllerType      string `json:"controller_type"`
	ControllerVariables string `json:"controller_variables"`
	AiName              string `json:"ai_name"`
	HumanName           string `json:"human_name"`
	Info                string `json:"info"`
	Options             string `json:"options"`
	ResponseLength      int    `json:"response_length"`
	Temperature         string `json:"temperature"`
	TopP                string `json:"top_p"`
	FrequencyPenalty    string `json:"frequency_penalty"`
	PresencePenalty     string `json:"presence_penalty"`
	Voice               string `json:"voice"`
}

func (s Scenario) String() string {
	return fmt.Sprintf(
		"(s%d) %s\nLevel: %d\nDuration: %d\nMission: %s\nCharacters: AI: %s, Human: %s\n\n%s\n\n%s",
		s.ID,
		s.Title,
		s.Level,
		s.Duration,
		s.Mission,
		s.AiName,
		s.HumanName,
		s.Information,
		s.Description,
	)
}

func (s *Server) Scenarios() ([]Scenario, error) {
	body, err := json.Marshal(ScenariosReq{Password: s.Password})
	if err != nil {
		return nil, err
	}
	resp, err := s.Client.Post(s.BaseURL.ResolveReference(urlScenarios).String(), "application/json", bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}
	re := make([]Scenario, 0)
	b, err := ioutil.ReadAll(resp.Body)
	err = json.NewDecoder(bytes.NewBuffer(b)).Decode(&re)
	if err != nil {
		return nil, err
	}
	return re, nil
}

type RefreshReq struct {
	ConversationId int    `json:"conversation_id"`
	Password       string `json:"password"`
	LogNumber      int    `json:"log_number"`
	UserID         string `json:"user_id"`
}

type RefreshResp struct {
	Response Message `json:"response"`
}

func (s *Server) Refresh(c Conversation, logNumber int, userID string) (RefreshResp, error) {
	body, err := json.Marshal(RefreshReq{
		ConversationId: c.ID,
		Password:       s.Password,
		LogNumber:      logNumber,
		UserID:         userID,
	})
	if err != nil {
		return RefreshResp{}, err
	}
	resp, err := s.Client.Post(s.BaseURL.ResolveReference(urlRefresh).String(), "application/json", bytes.NewBuffer(body))
	if err != nil {
		return RefreshResp{}, err
	}
	re := RefreshResp{}
	b, err := ioutil.ReadAll(resp.Body)
	err = json.NewDecoder(bytes.NewBuffer(b)).Decode(&re)
	if err != nil {
		return RefreshResp{}, err
	}
	return re, nil
}
