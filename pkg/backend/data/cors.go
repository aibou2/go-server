package data

import (
	_ "embed"
	"github.com/gin-contrib/cors"
	"github.com/pelletier/go-toml"
)

var CORSConfig cors.Config

//go:embed cors.toml
var corsConfig []byte

func init() {
	err := toml.Unmarshal(corsConfig, &CORSConfig)
	if err != nil {
		panic(err)
	}
}
