package data

import "time"
import _ "embed"

const Timeout = 1 * time.Hour

const MaxRefresh = 1 * time.Hour

//go:embed realm.txt
var Realm string

//go:embed secret_key.txt
var SecretKey []byte
