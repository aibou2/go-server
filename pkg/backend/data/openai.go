package data

import _ "embed"

//go:embed openai_api_key.txt
var OpenAIAPIKey string

//go:embed openai_org_key.txt
var openAIOrgKey string

var OpenAIOrgKey *string

func init() {
	if openAIOrgKey != "" {
		OpenAIOrgKey = &openAIOrgKey
	} // else, lease as is (nil)
}
