package types

import "github.com/google/uuid"

type CharacterType string

const (
	CharacterTypeAI        = "AI"
	CharacterTypeClient    = "Client"
	CharacterTypeNarration = "Narration"
)

type Character struct {
	Name string        `json:"name"`
	Type CharacterType `json:"type"`
}

type Characters map[uuid.UUID]Character

func (c Characters) Type(t CharacterType) (characters []uuid.UUID) {
	characters = make([]uuid.UUID, 0)
	for id, character := range c {
		if character.Type == t {
			characters = append(characters, id)
		}
	}
	return
}
