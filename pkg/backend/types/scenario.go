package types

import (
	"fmt"
	"time"
)

type GPT3Config struct {
	Engine           string         `json:"engine"`
	MaxTokens        *int           `json:"max_tokens"`
	Temperature      *float32       `json:"temperature"`
	TopP             *float32       `json:"top_p"`
	N                *int           `json:"n"`
	Stream           *bool          `json:"stream"`
	Logprobs         *int           `json:"logprobs"`
	Stop             *string        `json:"stop"`
	PresencePenalty  *float32       `json:"presence_penalty"`
	FrequencyPenalty *float32       `json:"frequency_penalty"`
	BestOf           *int           `json:"best_of"`
	LogitBias        map[string]int `json:"logit_bias"`
	Timeout          time.Duration  `json:"timeout"`
}

type Info map[string]string

type Scenario struct {
	Title       string        `json:"title"`
	Description string        `json:"description"`
	Duration    time.Duration `json:"duration"`
	Level       int           `json:"level"`
	Info        Info          `json:"info"`
	Messages    Messages      `json:"messages"`
	Characters  Characters    `json:"characters"`
	GPT3        GPT3Config    `json:"gpt_3"`
}

var _ fmt.Stringer = Scenario{}

func (s Scenario) String() string {
	return fmt.Sprintf(
		`%s

%s

Duration: %s
Level %d`,
		s.Title,
		s.Description,
		s.Duration,
		s.Level,
	)
}
