package types

import (
	"embed"
	"encoding/json"
	"fmt"
	"io/fs"
	"path/filepath"
)

//go:embed scenarios/*.json
var scenarios embed.FS

type ScenariosType map[string]Scenario

var _ fmt.Stringer = ScenariosType{}

func (scenarios ScenariosType) String() (s string) {
	for _, val := range scenarios {
		s += val.String()
	}
	return
}

var Scenarios ScenariosType

type ScenarioInvalid struct {
	Code   string
	Reason string
	Want   interface{}
	Got    interface{}
}

var _ error = ScenarioInvalid{}

func (s ScenarioInvalid) Error() string {
	return fmt.Sprintf("[%s] %s want %#v, got %#v", s.Code, s.Reason, s.Want, s.Got)
}

func verifyScenario(s Scenario) error {
	if s.GPT3.Engine == "" {
		return ScenarioInvalid{
			Code:   "0001",
			Reason: "GPT3.Engine",
			Want:   "(something not blank)",
			Got:    "",
		}
	}
	if s.GPT3.MaxTokens != nil && *s.GPT3.MaxTokens == 0 {
		return ScenarioInvalid{
			Code:   "0002",
			Reason: "GPT3.MaxTokens will fail",
			Want:   "(something not 0)",
			Got:    0,
		}
	}
	if s.Title == "" {
		return ScenarioInvalid{"0003", "Meta.Title", "(something not blank)", ""}
	}
	if s.Description == "" {
		return ScenarioInvalid{"0004", "Meta.Description", "(something not blank)", ""}
	}
	if s.Duration == 0 {
		return ScenarioInvalid{"0005", "Meta.Title", "(something not 0)", 0}
	}
	if s.Level == 0 {
		return ScenarioInvalid{"0006", "Meta.Level", "(something not 0)", 0}
	}
	if len(s.Characters) < 2 {
		return ScenarioInvalid{"0007", "Chars.len", "(something > 2)", len(s.Characters)}
	}
	return nil
}

func init() {
	// match scenarios
	matches, err := fs.Glob(scenarios, "scenarios/*.json")
	if err != nil {
		panic(fmt.Sprintf("glob: %s", err))
	}

	// define variables
	var data []byte
	Scenarios = map[string]Scenario{}

	for i, match := range matches {
		data, err = scenarios.ReadFile(match)
		if err != nil {
			panic(err)
		}

		path_ := filepath.Base(match)
		path := path_[:len(path_)-len(filepath.Ext(path_))]
		scenario := Scenario{}
		err = json.Unmarshal(data, &scenario)
		if err != nil {
			panic(fmt.Sprintf("%v %v json unmarshal: %s", i, match, err))
		}
		Scenarios[path] = scenario

		if err := verifyScenario(scenario); err != nil {
			panic(fmt.Sprintf("%v %v verify scenario: %s", i, match, err))
		}
	}
}
