package types

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/aibou2/go-server/pkg/backend/data"
	"gitlab.com/aibou2/go-server/pkg/backend/openai"
	"net/http"
)

type Message struct {
	From uuid.UUID `json:"from"`
	// sender of message

	VisibleTo []uuid.UUID `json:"visible_to"`
	// If VisibleTo is nil, visible to all

	EditableTo []uuid.UUID `json:"editable_to"`
	// If VisibleTo is nil, editable to all

	AnonymousTo []uuid.UUID `json:"anonymous_to"`
	// If VisibleTo is nil, anonymous to all

	Content string `json:"content"`
	// Content of message

	Rating *int `json:"rating"`
	// If Rating is nil, not rated
}

func (m Message) Render(characters Characters) string {
	return fmt.Sprintf("%s: %s", characters[m.From].Name, m.Content)
}

func (m Message) Rate(gpt3 GPT3Config) (int, error) {
	// OpenAI API Stuff
	client := openai.NewClient(
		&http.Client{Timeout: gpt3.Timeout},
		data.OpenAIAPIKey,
		data.OpenAIOrgKey,
	)
	return client.ContentFilter(context.Background(), m.Content, "test")
}

type Messages []Message

func (m Messages) Render(characters Characters) (rendered string) {
	for _, message := range m {
		rendered += message.Render(characters) + "\n"
	}
	// add prompt
	rendered += characters[characters.Type(CharacterTypeAI)[0]].Name + ":"
	return
}
