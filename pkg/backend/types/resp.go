package types

type Resp struct {
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}
