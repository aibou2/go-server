package types

import (
	"context"
	"github.com/google/uuid"
	"gitlab.com/aibou2/go-server/pkg/backend/data"
	"gitlab.com/aibou2/go-server/pkg/backend/openai"
	"golang.org/x/xerrors"
	"net/http"
	"time"
)

type Conversation struct {
	Scenario   Scenario   `json:"scenario"`
	Characters Characters `json:"characters"`
	Messages   Messages   `json:"messages"`
	Started    time.Time  `json:"started"`
}

var _ openai.Renderer = Conversation{}

func (c Conversation) Render() string {
	return c.Messages.Render(c.Characters)
}

func (c Conversation) Rate() (Conversation, error) {
	for i, message := range c.Messages {
		rating, err := message.Rate(c.Scenario.GPT3)
		if err != nil {
			return c, err
		}
		c.Messages[i].Rating = &rating
	}
	return c, nil
}

// Ask mutates its receiver.
func (c Conversation) Ask(ctx context.Context) (Conversation, error) {
	var character uuid.UUID
	characters := c.Characters.Type(CharacterTypeAI)

	switch len(characters) {
	case 0:
		return c, xerrors.Errorf("no AI characters found")
	case 1:
		character = characters[0]
	default: // (not 0) and (not 1)
		// TODO: decide whether to keep ambiguous or do another solution
		return c, xerrors.Errorf("more than one AI character found; ambiguous")
	}

	// OpenAI API Stuff
	client := openai.NewClient(
		&http.Client{Timeout: c.Scenario.GPT3.Timeout},
		data.OpenAIAPIKey,
		data.OpenAIOrgKey,
	)
	rendered := c.Render()
	completionResp, err := client.CreateCompletion(ctx, openai.CreateCompletionReq{
		Engine:           c.Scenario.GPT3.Engine,
		Prompt:           &rendered,
		MaxTokens:        c.Scenario.GPT3.MaxTokens,
		Temperature:      c.Scenario.GPT3.Temperature,
		TopP:             c.Scenario.GPT3.TopP,
		N:                c.Scenario.GPT3.N,
		Stream:           c.Scenario.GPT3.Stream,
		Logprobs:         c.Scenario.GPT3.Logprobs,
		Stop:             c.Scenario.GPT3.Stop,
		PresencePenalty:  c.Scenario.GPT3.PresencePenalty,
		FrequencyPenalty: c.Scenario.GPT3.FrequencyPenalty,
		BestOf:           c.Scenario.GPT3.BestOf,
		LogitBias:        c.Scenario.GPT3.LogitBias,
	})
	if err != nil {
		return c, xerrors.Errorf("completion: %w", err)
	}

	if len(completionResp.Choices) != 1 {
		// TODO: for 2+ choices, select from logprobs if available
		return c, xerrors.Errorf("wanted 1 choice, but got %v", len(completionResp.Choices))
	}
	choice := completionResp.Choices[0]

	rating, err := client.ContentFilter(ctx, choice.Text, "test")
	if err != nil {
		return c, xerrors.Errorf("content filter: %w", err)
	}

	c.Messages = append(c.Messages, Message{
		From:        character,
		VisibleTo:   nil,                  // visible to all
		EditableTo:  nil,                  // editable to all
		AnonymousTo: make([]uuid.UUID, 0), // anonymous to none
		Content:     choice.Text,
		Rating:      &rating,
	})

	//goland:noinspection ALL
	c, err = c.Rate()
	return c, err
}
