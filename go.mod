module gitlab.com/aibou2/go-server

go 1.16

// +heroku goVersion go1.16

require (
	github.com/appleboy/gin-jwt/v2 v2.6.4
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.1
	github.com/google/uuid v1.2.0
	github.com/jessevdk/go-flags v1.5.0
	github.com/line/line-bot-sdk-go v7.8.0+incompatible
	github.com/nicksnyder/go-i18n/v2 v2.1.2
	github.com/pelletier/go-toml v1.8.1
	github.com/piaohao/godis v0.0.18
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/colourdelete/go-aibou-server v1.1.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b // indirect
	golang.org/x/text v0.3.6
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
)
