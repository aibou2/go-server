GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
O_NAME=mybinary

SECRET_KEY="this_is_not_secure_make_sure_to_set_it_to_a_random_string_before_building"

all: fmt check coverage

metadata:
	date -Iseconds > pkg/data/built.txt
	git rev-parse HEAD > pkg/data/hash.txt
	git describe --abbrev=0 --always > pkg/data/version.txt
	git shortlog -snec --all > pkg/data/contributors.txt
	echo $(SECRET_KEY) > pkg/data/secret_key.txt
	echo $(REALM) > pkg/data/realm.txt
	go mod tidy
	rm -rf vendor

gen:
	go generate

pre_commit: gen check fmt coverage

fmt: metadata
	go fmt ./...

check: metadata
	go vet ./...

coverage: metadata gen
	go get github.com/boumenot/gocover-cobertura
	go test -race -coverprofile=coverage.txt -covermode atomic ./...
	gocover-cobertura -ignore-gen-files < coverage.txt > coverage.xml

build_line_dev: metadata gen
	go build -race -ldflags "-extldflags '-static'" -o $(CI_PROJECT_DIR)/bin/line gitlab.com/aibou2/go-server/cmd/line

build_line_release: metadata gen
	go build       -ldflags "-extldflags '-static'" -o $(CI_PROJECT_DIR)/bin/line gitlab.com/aibou2/go-server/cmd/line
